% This script is used as a test for qualisys-genom3 component. Make sure to
% be connected to `WifiVicon` and can ping `flylab-gs3` that has the
% following `IP` : `192.168.30.42`.
%
% `qualisys-genom3` should be built and running on your computer or on a
% jetson TX2 to be able to run this script.
%
% In addition, `genomixd` should be running in both cases to be able to
% communicate with the `qualisys-genom3` server through genomix clients.
%
% For more info on how to use `matlab-genomix` project, see this link:
% https://git.openrobots.org/projects/matlab-genomix/gollum/demo
%
%					Joudy Nader, 13/1/2022

clear;clc;

%% In case you are running the component on your PC.
client = genomix.client();
qualisys = client.load('qualisys');

%% % In case you are running the component on Jetson TX2.
% client = genomix.client('jetson2:10002');
% qualisys = client.load('qualisys','-i','mocap_mk2');

%% Creating data structure to be used with functions/activities.
% The naming of the members of the struct should be the same that in IDS of
% the component.
s = struct();

%% Setting which data to be exported from the qualisys server.
s.pubdata.bodies = 1;
s.pubdata.markers = 0;
s.pubdata.anon = 0;
qualisys.set_export(s);

%% Connecting to `flylab-gs3` and start receiving data.
s.host = '10.135.2.39';
s.bigEndian = 0;

qualisys.connect(s);

%% Listing the bodies present in the scene.
body_list = qualisys.body_list();

%% Reading the data of the first body.
if isempty(body_list.result.body_list) == 0 % Not empty.
    body_list.result.body_list{1}
    first_body_data = qualisys.bodies(body_list.result.body_list{1});
    first_body_data.bodies.pos
else
    print("No bodies are present in the scene...");
end
