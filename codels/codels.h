/*
 * Copyright (c) 2014-2019,2021 LAAS/CNRS
 * All rights reserved.
 *
 * Redistribution and use  in source  and binary  forms,  with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   1. Redistributions of  source  code must retain the  above copyright
 *      notice and this list of conditions.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice and  this list of  conditions in the  documentation and/or
 *      other materials provided with the distribution.
 *
 *                       Joudy Nader - January 2022
 *                       based on optitrack-genom3 from Anthony Mallet
 */
#ifndef H_QUALISYS_CODELS
#define H_QUALISYS_CODELS

#include "acqualisys.h"
#include "qualisys_c_types.h"

#include <RTProtocol.h>
#include <RTPacket.h>
#include <iostream>

struct qualisys_server_s {
  CRTProtocol rtProtocol;
};

struct qualisys_bodies_info_s {
  const struct tracker_descr *descr;
};

struct qualisys_log_s {
  FILE *f;
};

// structures to store tracker data: (taken from optitrack component)
struct tracker_anonmset {
  uint32_t n;			/* number of markers */
  struct position {
    float x, y, z;
  } *pos;			/* markers position */
};

struct tracker_msetdescr {
  char *name;			/* model name */
  char safename[64];		/* name without special chars */

  uint32_t nmnames;		/* number of marker names */
  char **lmname;			/* marker names */
};

struct tracker_mset {
  const struct tracker_msetdescr *descr;
  const char *name;		/* model name */
  struct tracker_anonmset mset;	/* markers */
};

struct tracker_bodydescr {
  char *name;			/* body name */
  char *rsname;			/* root segment name */
  char safename[64];		/* name without special chars */
  char rssafename[64];		/* rsname without special chars */
};

struct tracker_body {
  const struct tracker_bodydescr *descr;
  struct __attribute__ ((__packed__)) {
    float x, y, z, qx, qy, qz, qw;
  } pose;			/* body pose */

  struct tracker_anonmset mset;	/* associated marker set */
};

struct tracker_descr {
  uint32_t nmsets;	 	 /* number of marker sets descriptions */
  struct tracker_msetdescr *mset;/* marker sets descriptions */

  uint32_t nbodies;	 	 /* number of bodies descriptions */
  struct tracker_bodydescr *body;/* bodies descriptions */
};

struct tracker_data {
  //timestamp ? how to get vicon's one ?
  struct tracker_anonmset aset;  /* anonymous markers set */

  uint32_t nmsets;		/* number of markers sets */
  struct tracker_mset *mset;	/* markers sets */

  uint32_t nbodies;		/* number of rigid bodies */
  struct tracker_body *body;	/* rigid bodies */
};

genom_event	qualisys_create_ports(const struct tracker_descr *descr,
                                   const qualisys_ids_pubdata_s *pubdata,
                                   const qualisys_bodies *bodies,
                                   const qualisys_markers *markers,
                                   genom_context self);
genom_event	qualisys_update_ports(const struct tracker_data *data,
                                   qualisys_bodies_info_s *binfo,
                                   const qualisys_ids_pubdata_s *pubdata,
                                   const qualisys_ids_noise_s *noise,
                                   const qualisys_bodies *bodies,
                                   const qualisys_markers *markers,
                                   FILE *log, genom_context self);

#endif /* H_QUALISYS_CODELS */
