/*
 * Copyright (c) 2014-2019,2021 LAAS/CNRS
 * All rights reserved.
 *
 * Redistribution and use  in source  and binary  forms,  with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   1. Redistributions of  source  code must retain the  above copyright
 *      notice and this list of conditions.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice and  this list of  conditions in the  documentation and/or
 *      other materials provided with the distribution.
 *
 *                       Joudy Nader - January 2022
 *                       based on optitrack-genom3 from Anthony Mallet
 */
#include "acqualisys.h"
#include "codels.h"

#include "qualisys_c_types.h"
#include <fnmatch.h>

#define min(a, b) (a < b ? a : b)
#define max(a,b)  (a < b ? b : a)

// Global variable for counting iterations.
unsigned long g_iter = 0;

static size_t strnfilter(char *dst, size_t n, const char *src,
                           const char *accept, char repl);

void rotm2quat(float*, double&, double&, double&, double&);

/*
  The input is an array of 9 elements containing the rotation matrix (column major, provided by the SDK).
  The qw, qx, qy, qz values to be calculated and assigned at the end.
*/
void rotm2quat(float* rotm, double &qw, double &qx, double &qy, double &qz)
{
  /*
  This method is based on matlab's `rotm2quat` source code.
  You can find it here:
  https://github.com/robotology-legacy/mex-wholebodymodel/blob/master/mex-wholebodymodel/matlab/utilities/%2BWBM/%2Butilities/rotm2quat.m
  */

  float epsilon = 1e-12;

  // double quat[4];
  double s_inv = 0.;

  // Calculating the trace of the rotation matrix.
  float tr = rotm[0] + rotm[4] + rotm[8];
  if (tr > epsilon) // if tr > 0:
  {
    // scalar part:
    qw = 0.5 * sqrt(tr + 1);
    s_inv = 1 / (qw * 4);
    // vector part: (considering it's column major)
    qx = (rotm[5] - rotm[7]) * s_inv; // qx
    qy = (rotm[6] - rotm[2]) * s_inv; // qy
    qz = (rotm[1] - rotm[3]) * s_inv; // qz
  } 
  else // if tr <= 0, find the greatest diagonal element for calculating the scale factor s and the vector part of the quaternion:
  {
    if ((rotm[0] > rotm[4]) && (rotm[0] > rotm[8]))
    {
      qx = 0.5 * sqrt(rotm[0] - rotm[4] - rotm[8] + 1);
      s_inv = 1/(qx*4);

      qw = (rotm[5] + rotm[7]) * s_inv;
      qy = (rotm[1] + rotm[3]) * s_inv;
      qz = (rotm[2] + rotm[6]) * s_inv;
    }

    else
    {
      if(rotm[4] > rotm[8])
      {
        qy = 0.5 * sqrt(rotm[4] - rotm[8] - rotm[0] + 1);
        s_inv = 1 / (qy * 4);

        qw = (rotm[6] - rotm[2]) * s_inv;
        qx = (rotm[1] + rotm[3]) * s_inv;
        qz = (rotm[5] + rotm[7]) * s_inv;
      }

      else
      {
        qz = 0.5 * sqrt(rotm[8] - rotm[0] - rotm[4] + 1);
        s_inv = 1 / (qz * 4);

        qw = (rotm[1] - rotm[3])*s_inv;
        qx = (rotm[2] + rotm[6])*s_inv;
        qy = (rotm[5] + rotm[7])*s_inv;
      }
    }
  }
}

/* --- Task publish ----------------------------------------------------- */


/** Codel qualisys_publish_start of task publish.
 *
 * Triggered by qualisys_start.
 * Yields to qualisys_pause_descr.
 * Throws qualisys_e_nomem, qualisys_e_sdk.
 */
genom_event
qualisys_publish_start(qualisys_ids *ids, const genom_context self)
{
  ids->pubdata.bodies = true;
  ids->pubdata.markers = false;
  ids->pubdata.anon = false;

  ids->binfo = (qualisys_bodies_info_s*)calloc(1, sizeof(ids->binfo));
  if (!ids->binfo) return qualisys_e_nomem(self);

  ids->server = NULL;

  ids->noise.pstddev = 0.;
  ids->noise.qstddev = 0.;

  return qualisys_pause_descr;
}


/** Codel qualisys_publish_descr of task publish.
 *
 * Triggered by qualisys_descr.
 * Yields to qualisys_pause_descr, qualisys_sdkrecv,
 *           qualisys_taskdisconnect.
 * Throws qualisys_e_nomem, qualisys_e_sdk.
 */
genom_event
qualisys_publish_descr(qualisys_server_s **server,
                       qualisys_bodies_info_s **binfo,
                       const qualisys_ids_pubdata_s *pubdata,
                       const qualisys_bodies *bodies,
                       const qualisys_markers *markers,
                       const genom_context self)
{
  if(!server || !*server) return qualisys_pause_descr;

  // Ask qualisys sdk for descsription of bodies & markers (counts and names).
  unsigned int k, i;
  unsigned int n_subj, n_mkr; // n_subj : number of 6DOF bodies, n_mkr : number of markers.
  char name[64], rsname[64], mname[64]; // rsname: root segment name, mname: marker name.
  qualisys_e_sdk_detail e_sdk;
  size_t e_sdk_size = sizeof(e_sdk.what);
  struct tracker_descr *d;

  // If not already done, wait for connection (triggered in activity qualisys_connect_start).
  if( !(*server)->rtProtocol.Connected())
    return (genom_event) qualisys_pause_descr;

  CRTPacket::EPacketType packetType;
  CRTPacket *rtPacket;

  // Request a frame of data.
  if((*server)->rtProtocol.Receive(packetType, true) == CNetwork::ResponseType::success)
  {
    if(packetType == CRTPacket::PacketData)
    {
      rtPacket = (*server)->rtProtocol.GetRTPacket();

      // Get the number of subjects.
      n_subj = rtPacket->Get6DOFBodyCount();
    }
  }
  else
    return (genom_event) qualisys_e_sdk(&e_sdk, self);

  if((*binfo)->descr) { //Descriptor already exists.
    d = (struct tracker_descr*) (*binfo)->descr; // Read it.
  }
  else
    d = (tracker_descr*)calloc(1, sizeof(*d)); // Allocate new tracker_descr pointer variable.

  if(n_subj < d->nbodies) { // There's less subjects than last iteration, so let's free the memory not used anymore
    /*for(k=n_subj;k<d->nbodies;k++) {
      free(d->body[k].name);//this will not be used anymore
      free(d->body[k].rsname);
      free(d->mset[k].name);
      for(i=0;i<d->mset[k].nmnames;i++) {
        free(d->mset[k].lmname[i]);//marker name i of mset k
      }
      free(d->mset[k].lmname);//free the table of char*
      printf("freed pointers for body %d/%d\n", k, d->nbodies);///DEBUG
    }
    ////for some reason this makes the soft crash when happening (ie when an object disappears) !
    ////I don't understand why for now...
    ////maybe check that k is in the right range ? */
  } else if(n_subj > d->nbodies) {
    // There are newly detected 6DOF bodies, we should allocate memory to store descriptions.
    d->body = (tracker_bodydescr*)realloc(d->body, n_subj*sizeof(*d->body)); // Pointer to [tracker_bodydescr 1..n].
    d->mset = (tracker_msetdescr*)realloc(d->mset, n_subj*sizeof(*d->mset)); // Pointer to [tracker_msetdescr 1..n].
    if((!d->mset) || (!d->body)) { printf("publish_descr: alloc fail for the body/mset descriptor list\n"); return (genom_event) qualisys_e_nomem; }
  }

  // For each 6DOF body, get its name and the number of markers it has.
  for(unsigned int k = 0; k < min(n_subj,d->nbodies); k++) { // This loop only updates the list we already had before.
    // Save body's name in `name`.
    strncpy(name, (*server)->rtProtocol.Get6DOFBodyName(k), 64);
    strncpy(rsname, name, 64);

    // Get marker count in every subject.
    n_mkr = (*server)->rtProtocol.Get6DOFBodyPointCount(k);

    d->body[k].name = (char*)realloc(d->body[k].name, sizeof(name)*sizeof(char));
    d->body[k].rsname = (char*)realloc(d->body[k].rsname, sizeof(rsname)*sizeof(char)); // Copy `name` in `rsname`.
    if((!d->body[k].name) || (!d->body[k].rsname)) { printf("publish_descr: alloc fail for body %d\n", k); return (genom_event) qualisys_e_nomem; }

    if(n_mkr < d->mset[k].nmnames) { // there are less markers than before for this mset
      for(i=n_mkr;i<d->mset[k].nmnames;i++) { // so let's free corresponding names
        free(d->mset[k].lmname[i]);
      }
      //d->mset[k].lmname = realloc(d->mset[k].lmname, n_mkr*sizeof(char*));
      ///we could reallocate a smaller list but this is not required (last pointers will just not be used till necessary)
    } else if(n_mkr > d->mset[k].nmnames) {
      d->mset[k].lmname = (char**)realloc(d->mset[k].lmname, n_mkr*sizeof(char*));
    }

    if((!d->mset[k].lmname) || (!d->body[k].name) || (!d->body[k].rsname)) { printf("publish_descr: alloc fail for mset %d\n", k); return (genom_event) qualisys_e_nomem; }

    strncpy(d->body[k].name, name, sizeof(name)); // Store name.
    strncpy(d->body[k].rsname, rsname, sizeof(rsname)); // Store name.

    strnfilter(d->body[k].safename, sizeof(d->body[k].safename), name, "[a-zA-Z0-9_]", '_');
    strnfilter(d->body[k].rssafename, sizeof(d->body[k].rssafename), rsname, "[a-zA-Z0-9_]", '_');
    d->mset[k].name = d->body[k].name; // use the same name because couldn't get marker name in qualisys.
    strncpy(d->mset[k].safename, d->body[k].safename, sizeof(d->body[k].safename));//copy...

    // Now retrieve each marker name only if markers are exported.
    if(pubdata->markers)
    {
    for(i = 0;i < min(d->mset[k].nmnames, n_mkr); i++) { // This loop updates makers we already had.
      // Qualisys SDK not returning name of marker, for now save markers as `name${k}`
      // in condition that the object contains less than 10 markers strictly.
      d->mset[k].lmname[i] = (char*)realloc(d->mset[k].lmname[i], (1 + sizeof(name))*sizeof(char)); // Re-use last memory up to the size we had before
      strncpy(d->mset[k].lmname[i], name, sizeof(name)); // Store name.
      char i_char;
      sprintf(&i_char, "%d", i);
      strcat(d->mset[k].lmname[i], &i_char);
    }
    for(i = min(d->mset[k].nmnames, n_mkr); i < n_mkr; i++) { // This one is for the new markers
      d->mset[k].lmname[i] = (char*)calloc(sizeof(name)+1, sizeof(char)); // First allocation for the new space.
      if(!d->mset[k].lmname[i]) { printf("publish_descr: alloc fail for marker name %d of mset %d\n", i, k); return (genom_event) qualisys_e_nomem; }
      strncpy(d->mset[k].lmname[i], name, sizeof(name));//store name
      char i_char;
      sprintf(&i_char, "%d", i);
      strcat(d->mset[k].lmname[i], &i_char);
    }
    }
    d->mset[k].nmnames = n_mkr;
  }

  for(k = min(n_subj, d->nbodies); k < max(n_subj, d->nbodies); k++) { // Now do the same for new subjects.
    strncpy(name, (*server)->rtProtocol.Get6DOFBodyName(k), 64);
    strncpy(rsname, name, 64);

    n_mkr = (*server)->rtProtocol.Get6DOFBodyPointCount(k);

    d->body[k].name = (char*)calloc(sizeof(name), sizeof(char)); // First allocation.
    d->body[k].rsname = (char*)calloc(sizeof(rsname), sizeof(char));
    d->mset[k].lmname = (char**)calloc(n_mkr, sizeof(char*));
    if((!d->mset[k].lmname) || (!d->body[k].name) || (!d->body[k].rsname) ) { printf("publish_descr: new subject names alloc fail\n"); return (genom_event) qualisys_e_nomem; }

    strncpy(d->body[k].name, name, sizeof(name)); // Store name.
    strncpy(d->body[k].rsname, rsname, sizeof(rsname)); // Store rsname.

    strnfilter(d->body[k].safename, sizeof(d->body[k].safename), name, "[a-zA-Z0-9_]", '_');
    strnfilter(d->body[k].rssafename, sizeof(d->body[k].rssafename), rsname, "[a-zA-Z0-9_]", '_');
    d->mset[k].name = d->body[k].name; // Use the same data.
    strncpy(d->mset[k].safename, d->body[k].safename, sizeof(d->body[k].safename)); // Copy

    // Now retrieve each marker name, only if markers are exported.
    if(pubdata->markers)
    {
    for(i = 0; i < n_mkr; i++) {
      d->mset[k].lmname[i] = (char*)calloc(sizeof(name), sizeof(char)); // First allocation for the new space.
      if(!d->mset[k].lmname[i]) { printf("publish_descr: alloc fail for marker name %d of new mset %d\n", i, k); return (genom_event) qualisys_e_nomem; }
      strncpy(d->mset[k].lmname[i], name, sizeof(name)); // Store name.
      char i_char;
      sprintf(&i_char, "%d", i);
      strcat(d->mset[k].lmname[i], &i_char);
    }
    }

    d->mset[k].nmnames = n_mkr;
  }

  d->nbodies = n_subj;
  d->nmsets = n_subj;

  (*binfo)->descr = d; // push back descriptor (pointer)

  if (qualisys_create_ports((*binfo)->descr, pubdata, bodies, markers, self) != genom_ok) return (genom_event) qualisys_taskdisconnect;

  return qualisys_sdkrecv;
}

void free_data(struct tracker_data data) {
  // simply free data structure pointers
  // called when leaving sdkrecv before its end.
  // Make sure no `double free()` exception happens.
  for(int k = 0; k < data.nmsets; k++)
    if(data.mset[k].mset.pos != NULL)
    free(data.mset[k].mset.pos);
  if(data.mset != NULL)
  free(data.mset);
  if(data.body != NULL)
  free(data.body);

  if(data.aset.pos != NULL)
  free(data.aset.pos);
}

/** Codel qualisys_publish_sdkrecv of task publish.
 *
 * Triggered by qualisys_sdkrecv.
 * Yields to qualisys_descr, qualisys_sdkrecv.
 * Throws qualisys_e_nomem, qualisys_e_sdk.
 */
genom_event
qualisys_publish_sdkrecv(qualisys_server_s **server,
                         qualisys_bodies_info_s **binfo,
                         const qualisys_log_s *log,
                         const qualisys_ids_pubdata_s *pubdata,
                         const qualisys_ids_noise_s *noise,
                         const qualisys_bodies *bodies,
                         const qualisys_markers *markers,
                         const genom_context self)
{
  unsigned int n_subj, n_anon; // n_subj : Number of 6DOF bodies, n_anon : Number of unlabeled markers.
  qualisys_e_sdk_detail e_sdk;
  size_t e_sdk_size = sizeof(e_sdk.what);
  char *name, *rsname; // Variables name and root segment names.
  float out_x, out_y, out_z; // Used for quaternion of 6DOF bodies.
  float rotationMatrix[9]; // Used for quaternion of 6DOF bodies.
  double qw, qx, qy, qz; // Used for quaternion of 6DOF bodies.
  float m_x, m_y, m_z; // Used for marker 3D position.
  int occluded;
  struct tracker_data data;
  int k, i;
  unsigned int anon_id; // Unlabeled marker ID. Can be useful for tracking unlabeled markers maybe.

  // Request a frame of data.
  CRTPacket::EPacketType packetType;
  if((*server)->rtProtocol.Receive(packetType, true) != CNetwork::ResponseType::success) { return (genom_event) qualisys_e_sdk(&e_sdk, self);}

  // Check subject count.
  CRTPacket* rtPacket;
  if(packetType == CRTPacket::PacketData)
  {
    rtPacket = (*server)->rtProtocol.GetRTPacket();
    n_subj = rtPacket->Get6DOFBodyCount();

    // If user specified he wants unlabeled markers.
    if(pubdata->anon)
      n_anon = rtPacket->Get3DNoLabelsMarkerCount();
  }

  // Check unlabled markers count.
  data.aset.pos = (tracker_anonmset::position*)malloc(n_anon * 3 * sizeof(float));

  if(!(*binfo) || !(*binfo)->descr || (*binfo)->descr->nbodies != n_subj)
  {
    std::cout << "something changed: " << (*binfo)->descr->nbodies << " " << n_subj << std::endl;
    return (genom_event) qualisys_descr; // Something has changed : refetch descr.
  }

  // Local data structure.
  data.mset = (tracker_mset*)calloc((*binfo)->descr->nmsets, sizeof(*data.mset));
  data.body = (tracker_body*)calloc((*binfo)->descr->nbodies, sizeof(*data.body));

  if((!data.mset) || (!data.body) ) { // Couldn't allocate memory for data structures.
    printf("publish_sdkrecv: alloc fail for data structure\n");
    free_data(data);
    return (genom_event) qualisys_e_nomem;
  }

  // For each subject, retrieve pose and its markers' position.
  for(k = 0; k < n_subj; k++) 
  {
    name = (*binfo)->descr->body[k].name;
    rsname = (*binfo)->descr->body[k].rsname;

    if(!rtPacket->Get6DOFBody(k, out_x, out_y, out_z, rotationMatrix))
    {
      free_data(data);
      return (genom_event) qualisys_descr;
    }

    // Qualisys SDK returns rotation as a rotation matrix, we use this function to transform
    // rotation matrix into a quaternion.
    rotm2quat(rotationMatrix, qw, qx, qy, qz);

    data.body[k].descr   = &((*binfo)->descr->body[k]);
    // Show on the terminal when a body is occluded, so that we know.
    if(std::isnan(out_x) && std::isnan(out_y) && std::isnan(out_z))
    {
      std::cout << "Iteration: " << g_iter << " " << name << " is occluded or not present in the scene!" << std::endl;
    }

    data.body[k].pose.x  = out_x * 0.001;
    data.body[k].pose.y  = out_y * 0.001;
    data.body[k].pose.z  = out_z * 0.001;
    data.body[k].pose.qw = qw;
    data.body[k].pose.qx = qx;
    data.body[k].pose.qy = qy;
    data.body[k].pose.qz = qz;

    data.mset[k].descr = &((*binfo)->descr->mset[k]);
    data.mset[k].name = (*binfo)->descr->mset[k].name;
    data.mset[k].mset.n = (*binfo)->descr->mset[k].nmnames;
    data.mset[k].mset.pos = (tracker_anonmset::position*)calloc(data.mset[k].mset.n, sizeof(*data.mset[k].mset.pos));

    if(!data.mset[k].mset.pos) { // Couldn't allocate memory for marker set position data structure.
      printf("publish_sdkrecv: alloc fail for mset[%d].pos\n", k);
      free_data(data);
      return (genom_event) qualisys_e_nomem;
    }

    if(pubdata->markers) // Only retrieve labeled markers data if export() is requested with markers.
    {
      for(i = 0; i < data.mset[k].mset.n; i++) {
        if(!rtPacket->Get3DMarker(i, m_x, m_y, m_z))
        {
          std::cout << "returning to qualisys_descr" << std::endl;
          return (genom_event) qualisys_descr; /*qualisys_e_sdk(&e_sdk, self);*/   /// if something's wrong, try to refetch descr
        }
        data.mset[k].mset.pos[i].x = m_x * 0.001;
        data.mset[k].mset.pos[i].y = m_y * 0.001;
        data.mset[k].mset.pos[i].z = m_z * 0.001;
        // TODO: also output occluded state (?)
      }
      data.body[k].mset = data.mset[k].mset; // Copying the final marker set data structure to the corresponding body `mset`.
    }
  }

  // Anonymous markers 
  if(pubdata->anon) // Only retrieve unlabeled markers data if export() is requested with anon.
  {
    for(i=0; i < n_anon; i++)
    {
      if(!rtPacket->Get3DNoLabelsMarker(i, m_x, m_y, m_z, anon_id))
      {
        free_data(data);
        return (genom_event) qualisys_descr;
      }

      data.aset.pos[i].x = m_x * 0.001;
      data.aset.pos[i].y = m_y * 0.001;
      data.aset.pos[i].z = m_z * 0.001;
    }
  }

  data.nmsets = n_subj;
  data.nbodies= n_subj;
  data.aset.n = n_anon;

  // Output data to ports.
  qualisys_update_ports(&data, *binfo, pubdata, noise, bodies, markers, log?log->f:NULL, self);

  // Data have been copied inside update_ports, so free it now.
  free_data(data);

  g_iter++;

  return (genom_event) qualisys_sdkrecv;
}


/** Codel qualisys_publish_taskdisconnect of task publish.
 *
 * Triggered by qualisys_taskdisconnect.
 * Yields to qualisys_pause_descr.
 * Throws qualisys_e_nomem, qualisys_e_sdk.
 */
genom_event
qualisys_publish_taskdisconnect(qualisys_server_s **server,
                                const genom_context self)
{
  qualisys_e_sdk_detail e_sdk;
  size_t e_sdk_size=sizeof(e_sdk.what);

  (*server)->rtProtocol.Disconnect();

  if((*server)->rtProtocol.Connected())
    printf("Server is still connected...\n");

  // Freeing server variable (allocated with new).
  delete (*server);

  return (genom_event) qualisys_pause_descr;
}


/** Codel qualisys_publish_stop of task publish.
 *
 * Triggered by qualisys_stop.
 * Yields to qualisys_ether.
 * Throws qualisys_e_nomem, qualisys_e_sdk.
 */
genom_event
qualisys_publish_stop(qualisys_bodies_info_s **binfo,
                      qualisys_server_s **server,
                      const genom_context self)
{
  // Disconnecting the server.
  if((*server))
    qualisys_publish_taskdisconnect(server, self);

  // Freeing binfo data structure.
  free(*binfo);

  return qualisys_ether;
}


/* --- Activity ping ---------------------------------------------------- */

/** Codel qualisys_ping_start of activity ping.
 *
 * Triggered by qualisys_start.
 * Yields to qualisys_send.
 * Throws qualisys_e_nomem, qualisys_e_sdk.
 */
genom_event
qualisys_ping_start(const qualisys_server_s *server,
                    const qualisys_ids_ping_ctx *ping, int32_t *id,
                    double *ts, int32_t *retries,
                    const genom_context self)
{
  /* skeleton sample: insert your code */
  /* skeleton sample */ return qualisys_send;
}

/** Codel qualisys_ping_send of activity ping.
 *
 * Triggered by qualisys_send.
 * Yields to qualisys_pause_sleep.
 * Throws qualisys_e_nomem, qualisys_e_sdk.
 */
genom_event
qualisys_ping_send(const qualisys_server_s *server,
                   const genom_context self)
{
  /* skeleton sample: insert your code */
  /* skeleton sample */ return qualisys_pause_sleep;
}

/** Codel qualisys_ping_sleep of activity ping.
 *
 * Triggered by qualisys_sleep.
 * Yields to qualisys_send, qualisys_stop.
 * Throws qualisys_e_nomem, qualisys_e_sdk.
 */
genom_event
qualisys_ping_sleep(const qualisys_ids_ping_ctx *ping, int32_t id,
                    int32_t *retries, const genom_context self)
{
  /* skeleton sample: insert your code */
  /* skeleton sample */ return qualisys_send;
}

/** Codel qualisys_ping_stop of activity ping.
 *
 * Triggered by qualisys_stop.
 * Yields to qualisys_ether.
 * Throws qualisys_e_nomem, qualisys_e_sdk.
 */
genom_event
qualisys_ping_stop(const qualisys_ids_ping_ctx *ping, double ts,
                   double *rtt, char info[64],
                   const genom_context self)
{
  /* skeleton sample: insert your code */
  /* skeleton sample */ return qualisys_ether;
}


/* --- Activity connect ------------------------------------------------- */

/** Codel qualisys_connect_start of activity connect.
 *
 * Triggered by qualisys_start.
 * Yields to qualisys_ether.
 * Throws qualisys_e_nomem, qualisys_e_sdk.
 */
genom_event
qualisys_connect_start(const char host[128], uint16_t host_port,
                       uint16_t udpPort, uint16_t majorVersion,
                       uint16_t minorVersion, bool bigEndian,
                       const qualisys_ids_pubdata_s *pubdata,
                       qualisys_server_s **server,
                       const genom_context self)
{
  qualisys_e_sdk_detail e_sdk;
  size_t e_sdk_size = sizeof(e_sdk.what);

  // Here, we allocate memory for `server` using `new` to ensure
  // constructor of `rtProtocol` is called.
  // Problems were encoutered when using `calloc`. To be investigated.
  *server = new qualisys_server_s;

  if((server) && (*server) && (*server)->rtProtocol.Connected()) {
    printf("connect_start: Qualisys client is already connected!");
    return qualisys_ether;
  }

  if(!(*server)->rtProtocol.Connect(host, host_port, &udpPort, majorVersion, minorVersion, bigEndian)) {
    return qualisys_e_sdk(&e_sdk, self); // Should solve e_sdk issue (in Qualisys)
  }

  if(!(*server)->rtProtocol.Connected()) {
    printf("connect_start: Qualisys client is not connected !");
    return qualisys_e_sdk(&e_sdk, self); // Should solve e_sdk issue (in Qualisys)
  }

  bool dataAvailable = false;
  if(!(*server)->rtProtocol.Read6DOFSettings(dataAvailable))
  {
    printf("connect_start: Read6DOFSettings() error: %s\n\n", (*server)->rtProtocol.GetErrorString());
    return qualisys_e_sdk(&e_sdk, self);
  }

  // This should be tested if it works or not when markers and unlabeld markers are to be exported.
  // What we are doing here is a test to see if the user requested to publish labeled and
  // unlabeled markers using `set_export()` function.
  // `set_export()` will change the value of the flags in `pubdata`, and depending on the flags
  // we should call `streamFrames()` of `CRTProtocol` class accordingly, changing the `component` variable.
  unsigned int nComponentType = (pubdata->bodies ? CRTProtocol::cComponent6d : 0) |
                                (pubdata->markers ? CRTProtocol::cComponent3d : 0) |
                                (pubdata->anon ? CRTProtocol::cComponent3dNoLabels : 0);

  if(!(*server)->rtProtocol.StreamFrames(CRTProtocol::RateAllFrames, 0, udpPort, NULL, nComponentType))
  {
    printf("rtProtocol.StreamFrames: %s\n\n", (*server)->rtProtocol.GetErrorString());
    return qualisys_e_sdk(&e_sdk, self);
  }

  printf("connect_start: connection and initialisation succeeded\n");

  return qualisys_ether;
}


/* --- Activity disconnect ---------------------------------------------- */

/** Codel qualisys_disconnect of activity disconnect.
 *
 * Triggered by qualisys_start.
 * Yields to qualisys_ether.
 * Throws qualisys_e_nomem, qualisys_e_sdk.
 */
genom_event
qualisys_disconnect(qualisys_server_s **server,
                    const genom_context self)
{
  qualisys_publish_taskdisconnect(server, self);
  return (genom_event) qualisys_ether;
}


/* --- strnfilter ---------------------------------------------------------- */

static size_t
strnfilter(char *dst, size_t n, const char *src, const char *accept, char repl)
{
  char x[2];

  x[1] = '\0';
  while(--n && *src) {
    x[0] = *src++;
    if (fnmatch(accept, x, 0))
      *dst++ = repl;
    else
      *dst++ = x[0];
  }
  *dst = '\0';
  return n;
}
