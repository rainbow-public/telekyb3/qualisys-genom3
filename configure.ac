
dnl Autoconf file for building qualisys codels library.
dnl
dnl Copyright (c) 2022 IRISA/Inria
dnl All rights reserved.
dnl
dnl Redistribution and use  in source  and binary  forms,  with or without
dnl modification, are permitted provided that the following conditions are
dnl met:
dnl
dnl   1. Redistributions of  source  code must retain the  above copyright
dnl      notice and this list of conditions.
dnl   2. Redistributions in binary form must reproduce the above copyright
dnl      notice and  this list of  conditions in the  documentation and/or
dnl      other materials provided with the distribution.
dnl
dnl

AC_PREREQ(2.59)

AC_INIT([qualisys-genom3],[1.0],[])
AC_CONFIG_MACRO_DIR([autoconf])
AC_CONFIG_AUX_DIR([autoconf])
AC_CONFIG_HEADERS([autoconf/acqualisys.h])
AM_INIT_AUTOMAKE([foreign no-define])

dnl Compilers
dnl
LT_INIT([disable-static])
AC_PROG_CC
AC_PROG_CXX


dnl Require GNU make
AC_CACHE_CHECK([for GNU make], [ac_cv_path_MAKE],
  [AC_PATH_PROGS_FEATURE_CHECK([MAKE], [make gmake],
    [case `$ac_path_MAKE --version 2>/dev/null` in
       *GNU*) ac_cv_path_MAKE=$ac_path_MAKE; ac_path_MAKE_found=:;;
     esac],
    [AC_MSG_ERROR([could not find GNU make])])])
AC_SUBST([MAKE], [$ac_cv_path_MAKE])


dnl External packages
PKG_CHECK_MODULES(requires, [
  openrobots2-idl >= 2.0
  genom3 >= 2.99.39
])

# Dependency on 'qualisys_cpp_sdk'. Check that RTProtocol.h and class
# RTProtocol in C++ libqualisys_cpp_sdk.so are present.
AC_LANG([C++])
CPPFLAGS="$CPPFLAGS -I/usr/local/include/qualisys_cpp_sdk"
AC_CHECK_HEADER([RTProtocol.h],,[
  AC_MSG_ERROR([Could not find RTProtocol.h file.])])

AC_MSG_CHECKING([CRTProtocol in libqualisys_cpp_sdk])
SAVED_LDFLAGS="$LDFLAGS"
LDFLAGS="$LDFLAGS -lqualisys_cpp_sdk"
AC_LINK_IFELSE(
  [AC_LANG_PROGRAM([#include <RTProtocol.h>], [CRTProtocol dummy()])],
  [AC_MSG_RESULT([yes])],
  [AC_MSG_RESULT([no])
  AC_MSG_ERROR([Could not find libqualisys_cpp_sdk dependency library.])])
LDFLAGS="$SAVED_LDFLAGS"

AC_PATH_PROG(GENOM3, [genom3], [no])
if test "$GENOM3" = "no"; then
  AC_MSG_ERROR([genom3 tool not found], 2)
fi

dnl --with-templates option
AG_OPT_TEMPLATES([$GENOM3 ],
    [$srcdir/qualisys.gen])

dnl Doc
AM_MISSING_PROG([ASCIIDOCTOR], [asciidoctor])

dnl Output
AC_CONFIG_FILES([
	qualisys-genom3.pc
	qualisys-genom3-uninstalled.pc
	Makefile
	codels/Makefile
])
AC_OUTPUT
AG_OUTPUT_TEMPLATES
